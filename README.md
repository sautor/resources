# Sautor Resources

Extensão de rescursos para o sistema Sautor.

## Instalação

Instalar via Composer:

````sh
composer require sautor/resources
````

No final do ficheiro `resources/scss/app.scss`:

````scss
// Addons
@import "@sautor/resources/resources/scss/styles";
````
