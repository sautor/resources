@props(['resources', 'grupo'])

<div class="sautor-list">
    @foreach($resources as $resource)
        <div class="sautor-list__item">
            <div class="sautor-list__item__logo">
                            <span class="sautor-list__item__logo__icon sautor-list__item__logo__icon--strong">
                                <span class="fad fa-{{ $resource->icon }}"></span>
                            </span>
            </div>
            <button @click.prevent="openModal('{{ $resource->key }}Modal')" class="sautor-list__item__data">
                <p class="sautor-list__item__name">{{ $resource->label }}</p>
            </button>
            <div class="sautor-list__item__actions">
                <button @click.prevent="openModal('{{ $resource->key }}Modal')">
                    <span class="far fa-angle-right"></span>
                </button>
            </div>
        </div>
    @endforeach
</div>


@foreach($resources as $resource)
    <modal id="{{ $resource->key }}Modal" @if($resource->isLargeModal()) class="modal--lg" @endif >
        <form action="{{ route('resources.resource', empty($grupo) ? $resource->key : [$resource->key, $grupo]) }}" target="_blank">
            <div class="modal__body">
                <h3 class="modal__title">
                    {{ $resource->label }}
                </h3>

                @foreach($resource->getOptions() as $option)

                    @if($option['type'] === 'checkbox')
                        <x-forms.checkbox :name="$option['name']" :label="$option['label']" :id="$resource->key.'-'.$option['name']" />
                    @elseif($option['type'] === 'checkbox-group')
                        <div class="field">
                            <label class="label">{{ $option['label'] }}</label>
                            <div class="s-res-pl-2">
                                @foreach($option['options'] as $cbVal => $label)
                                    <x-forms.checkbox :name="$option['name'].'[]'" :label="$label" :id="$resource->key.'-'.$option['name'].'-'.$cbVal" :cbVal="$cbVal" />
                                @endforeach
                            </div>

                        </div>
                    @endif

                @endforeach

                @if($resource->getOptionsView())
                    @includeIf($resource->getOptionsView())
                @endif
            </div>

            <footer class="modal__footer">
                <button type="submit" class="button button--primary">Abrir</button>
                <button type="button" class="button" @click.prevent="closeModal('{{ $resource->key }}Modal')">Fechar</button>
            </footer>
        </form>
    </modal>
@endforeach
