@extends('layouts.group-admin')

@section('group-content')

    <div class="group-page__header">
        <h1 class="group-page__header__title">
            Recursos
        </h1>
    </div>

    @if($commonResources->isNotEmpty())
        <h3 class="title title--sm s-res-mb-2">Comuns</h3>

        <x-resources::list :resources="$commonResources" />
    @endif

    @if($groupResources->isNotEmpty())
        <h3 class="title title--sm s-res-mt-4 s-res-mb-2">Grupo</h3>

        <x-resources::list :resources="$groupResources" :grupo="$grupo" />
    @endif

@endsection

@push('styles')
    <link rel="stylesheet" href="{{ asset('addons/resources/styles.css') }}">
@endpush
