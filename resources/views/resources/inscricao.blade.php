@extends('resources::layout')

@section('title')
  Ficha de Inscrição - {{ $grupo->nome_curto }}
@endsection

@section('content')
  @include('resources::resources.partials.header')

  <h1 class="title print__title">
    Ficha de inscrição
    @if(array_key_exists('anoLetivo', $query))
      <small>{{ Sautor\formatAnoLetivo(Sautor\anoLetivo()) }}</small>
    @endif
  </h1>

  <div class="print-inscricao__section print-inscricao__campos print-placeholders--large">
    <span class="print-placeholder print-placeholder--w100" data-label="Nome completo"></span>
    <span class="print-placeholder print-placeholder--one-third" data-label="N.º Identificação Civil"></span>
    <span class="print-placeholder print-placeholder--one-third" data-label="N.º Utente de Saúde"></span>
    <span class="print-placeholder print-placeholder--one-third" data-label="N.º de Identificação Fiscal"></span>
    <span class="print-placeholder print-placeholder--three-quarters" data-label="Profissão"></span>
    <span class="print-placeholder print-placeholder--one-quarter" data-label="Data de nascimento"></span>

    <span class="print-placeholder print-placeholder--w100 mt-5" style="height: 4rem;" data-label="Morada"></span>
    <span class="print-placeholder print-placeholder--three-quarters" data-label="Endereço de e-mail"></span>
    <span class="print-placeholder print-placeholder--one-quarter" data-label="N.º de telefone"></span>
  </div>

  <div class="print-inscricao__section">
    <h4 class="title title--sm">Passos</h4>

    <div class="print-inscricao__campos print-placeholders--large">
      <span class="print-placeholder print-placeholder--one-third" data-label="Data de Batismo"></span>
      <span class="print-placeholder print-placeholder--one-third" data-label="Data da 1ª Comunhão"></span>
      <span class="print-placeholder print-placeholder--one-third" data-label="Data da Confirmação"></span>
      <span class="print-placeholder print-placeholder--one-third" data-label="Local"></span>
      <span class="print-placeholder print-placeholder--one-third" data-label="Local"></span>
      <span class="print-placeholder print-placeholder--one-third" data-label="Local"></span>
    </div>
  </div>

  @unless(array_key_exists('semFamiliares', $query))

  <div class="print-inscricao__section">
    <h4 class="title title--sm">Familiares</h4>

    <div class="print-inscricao__familiares__list">
    @for ($i = 0; $i < 2; $i++)
      <div class="print-inscricao__campos print-placeholders--large">
        <span class="print-placeholder print-placeholder--w100" data-label="Nome completo"></span>
        <span class="print-placeholder print-placeholder--half" data-label="N.º Identificação Civil"></span>
        <span class="print-placeholder print-placeholder--half" data-label="Data de nascimento"></span>
        <span class="print-placeholder print-placeholder--half" data-label="Relação (pai/mãe…)"></span>
        <span class="print-placeholder print-placeholder--half" data-label="N.º de telefone"></span>
        <span class="print-placeholder print-placeholder--w100" data-label="Endereço de email"></span>
        <span class="print-placeholder print-placeholder--w100" data-label="Profissão"></span>
      </div>
    @endfor
    </div>
  </div>

  @endunless

  <div class="print-inscricao__section">
    <h4 class="title title--sm">Outras informações</h4>

    <div class="print-inscricao__campos print-placeholders--large">
      @php($fields = Setting::get(implode('-', ['addon', 'resources', $grupo->id, 'setting', 'registration-fields'])))
      @unless(empty($fields))
        {!! Sautor\Resources\Helpers\replacePlaceholders($fields) !!}
      @endunless
      <span class="print-placeholder print-placeholder--w100" style="height: 5rem;" data-label="Observações"></span>
    </div>
  </div>


@endsection
