@extends('resources::layout')

@section('title')
  Lista de membros - {{ $grupo->nome_curto }}
@endsection

@section('content')
  @include('resources::resources.partials.header')

  <div class="print-lista__content">
    <h1 class="title print__title">
      {{ $grupo->nome }}
      @if(array_key_exists('anoLetivo', $query))
        <small>{{ Sautor\formatAnoLetivo(Sautor\anoLetivo()) }}</small>
      @endif
    </h1>

    <?php
      $fields_mapping = [
        'data_nascimento' => 'Data de nascimento',
        'idade' => 'Idade',
        'nic' => 'Nº Id. Civil',
        'nif' => 'Nº Id. Fiscal',
        'nus' => 'Nº Utente Saúde',
        'email' => 'Endereço de e-mail',
        'telefone' => 'Telefone',
        'profissao' => 'Profissão',
        'genero' => 'Género',
      ];

      $formatters = [
        'data_nascimento' => function ($d) { return $d->isoFormat('LL'); }
      ];

      $fields = array_key_exists('fields', $query) ? $query['fields'] : [];

      $inscritos_query = $grupo->inscritos();
      if (array_key_exists('semResponsaveis', $query)) {
          $inscritos_query = $inscritos_query->wherePivot('responsavel', false);
      }
      $inscritos = $inscritos_query->get();

      $is_small = $inscritos->count() > 15;

      $py = $is_small ? 's-res-py-2' : 's-res-py-3';
    ?>

    <table class="s-res-w-full s-res-border-collapse">
      <thead class="s-res-bg-gray-100 s-res-text-gray-600">
      <tr>
        <th class="s-res-uppercase s-res-font-bold s-res-text-sm s-res-py-3 s-res-px-2 s-res-text-left">Nome</th>
        @foreach($fields as $f)
          <th class="s-res-uppercase s-res-font-bold s-res-text-sm s-res-py-3 s-res-px-2 s-res-text-left">{{ $fields_mapping[$f] }}</th>
        @endforeach
      </tr>
      </thead>
      <tbody>
      @foreach($inscritos as $pessoa)
        <tr>
          <td class="s-res-border-t {{ $py }} s-res-px-2 s-res-font-accent">{{ $pessoa->nome_exibicao }}</td>
          @foreach($fields as $f)
            @php($formatter = @$formatters[$f])
            <td class="s-res-border-t {{ $py }} s-res-px-2">
              {{ $formatter && $pessoa->{$f} ? $formatter($pessoa->{$f}) : $pessoa->{$f} }}
            </td>
          @endforeach
        </tr>
      @endforeach
      </tbody>
    </table>
  </div>

@endsection
