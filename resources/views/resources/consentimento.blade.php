@extends('resources::layout')

@section('title')
  Autorização para tratamento de dados pessoais
@endsection

@section('content')
  @include('resources::resources.partials.logo')

  <div class="print-consent__authorization">
    <h1 class="title print__title">Autorização para tratamento de dados pessoais</h1>

    <p>{!! Sautor\Resources\Helpers\replacePlaceholders(Setting::get('gdpr-authorization-'.(array_key_exists('menor',$query) ? 'minor' : 'adult'))) !!}</p>

    <div class="print-placeholder print-placeholder--signature" data-label="Assinatura"></div>
  </div>

  <div class="print-consent__info">
    {!! Setting::get('gdpr-info') !!}
  </div>
@endsection
