@if($grupo->logo_horizontal)
    <img src="{{ url($grupo->logo_horizontal) }}" class="print__logo print__logo--horizontal">
@elseif($grupo->logo)
    <img src="{{ url($grupo->logo) }}" class="print__logo">
@else
    <h2 class="print__grupo">
        {{ $grupo->nome }}
    </h2>
@endif
