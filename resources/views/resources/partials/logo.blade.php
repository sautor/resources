@if(Setting::get('site-logo'))
    <img src="{{ Setting::get('site-logo') }}" alt="{{ config('app.name') }}" class="print__logo">
@else
    <h1 class="print__name">{{ config('app.name') }}</h1>
@endif
