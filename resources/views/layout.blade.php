<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>
        @yield('title')
        - {{ config('app.name') }}
    </title>

    <!-- Styles -->
    @vite(['resources/sass/main.scss'])
    <link href="{{ asset('addons/resources/styles.css') }}" rel="stylesheet">
    @include('layouts.partials.custom-styles')
    @stack('styles')
</head>
<body class="{{ implode(" ", $bodyClass) }}">

<div class="print-area">
    <div class="print-container s-res-p-8 s-res-mx-auto">
        @yield('content')

        @empty($hideFooter)
        <div class="print__footer">
            Modelo de <strong>@yield('title')</strong> em {{ config('app.name') }}
            a {{ \Carbon\Carbon::now()->isoFormat('LL') }}
        </div>
        @endempty
    </div>
</div>

</body>
</html>
