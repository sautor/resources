<?php

use Sautor\Resources\Controllers\ResourcesController;

Route::middleware(['web', 'maintenance', 'auth', 'addon:resources'])->name('resources.')->namespace('Sautor\Resources\Controllers')->group(function () {
    Route::get('recursos/{key}/{grupo?}', 'ResourcesController@resource')->name('resource');
});

Sautor\groupRoute(function () {
    Route::middleware(['auth', 'addon:resources'])->name('resources.')->group(function () {
        Route::get('recursos', [ResourcesController::class, 'grupo'])->name('index');
    });
});
