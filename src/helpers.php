<?php

namespace Sautor\Resources\Helpers;

function replacePlaceholders($string)
{
    return preg_replace('/\[(.*?):(.*?)\]/m', '<span class="print-placeholder print-placeholder--$1" data-label="$2"></span>', $string);
}
