<?php

namespace Sautor\Resources;

use Sautor\Core\Models\Grupo;
use Sautor\Core\Services\Addons\Addon;

class Resource
{
    public string $key;

    public string $label;

    public string $icon;

    public string $view;

    public ?string $description;

    private $format = 'A4';

    private bool $groupResource = false;

    private array $options = [];

    private ?string $optionsView = null;

    private bool $largeModal = false;

    private ?string $addon = null;

    /**
     * Resource constructor.
     */
    private function __construct(string $key, string $label, string $icon, string $view, ?string $description = null)
    {
        $this->key = $key;
        $this->label = $label;
        $this->icon = $icon;
        $this->view = $view;
        $this->description = $description;
    }

    public function getFormat($query = []): string
    {
        return is_callable($this->format) ? ($this->format)($query) : $this->format;
    }

    public function setFormat($format): Resource
    {
        $this->format = $format;

        return $this;
    }

    public function isGroupResource(): bool
    {
        return $this->groupResource;
    }

    public function setGroupResource(bool $groupResource): Resource
    {
        $this->groupResource = $groupResource;

        return $this;
    }

    public function getOptions(): array
    {
        return $this->options;
    }

    public function setOptions(array $options): Resource
    {
        $this->options = $options;

        return $this;
    }

    public function getOptionsView(): ?string
    {
        return $this->optionsView;
    }

    public function setOptionsView(?string $optionsView): Resource
    {
        $this->optionsView = $optionsView;

        return $this;
    }

    public function isLargeModal(): bool
    {
        return $this->largeModal;
    }

    public function setLargeModal(bool $largeModal): Resource
    {
        $this->largeModal = $largeModal;

        return $this;
    }

    public function getAddon(): ?Addon
    {
        if (! $this->addon) {
            return null;
        }

        return \AddonService::get($this->addon);
    }

    public function setAddon(Addon $addon): Resource
    {
        $this->addon = $addon->key;

        return $this;
    }

    public function isVisibleFor(?Grupo $grupo = null): bool
    {
        if (! $this->addon) {
            return true;
        }
        if ($this->isGroupResource() && $grupo) {
            return $this->getAddon()->isEnabledFor($grupo);
        }

        return $this->getAddon()->isEnabled();
    }

    public static function create(string $key, string $label, string $icon, string $view, ?string $description = null): Resource
    {
        return new Resource($key, $label, $icon, $view, $description);
    }
}
