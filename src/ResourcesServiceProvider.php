<?php

namespace Sautor\Resources;

use Illuminate\Support\ServiceProvider;
use Sautor\Core\Services\Addons\Addon;

class ResourcesServiceProvider extends ServiceProvider
{
    private Addon $addon;

    /**
     * PaymentsServiceProvider constructor.
     *
     * @param  \Illuminate\Contracts\Foundation\Application  $app
     */
    public function __construct($app)
    {
        parent::__construct($app);
        $this->addon = Addon::create('resources', 'Recursos', 'clipboard', 'Extensão com recursos para o sistema e os grupos.')
            ->setEntryRouteForGroup('resources.index')
            ->setGroupSettings([
                ['type' => 'textarea', 'name' => 'registration-fields', 'label' => 'Campos adicionais para a ficha de inscrição'],
            ])
            ->withAssets();
    }

    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__.'/../resources/views', 'resources');
        $this->loadRoutesFrom(__DIR__.'/../routes/web.php');

        $addonService = $this->app->make('AddonService');
        $addonService->register($this->addon);

        // Register initial resources
        $this->registerResources();
    }

    /**
     * Register any package services.
     *
     * @return void
     */
    public function register()
    {
        // Register the service the package provides.
        $this->app->singleton('resources', function ($app) {
            return new Resources;
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['resources'];
    }

    private function registerResources()
    {
        $registry = $this->app->make('resources');
        $registry->register(
            Resource::create('inscricao', 'Ficha de inscrição', 'file-user', 'resources::resources.inscricao')
                ->setGroupResource(true)
                ->setOptions([
                    ['type' => 'checkbox', 'label' => 'Sem familiares', 'name' => 'semFamiliares'],
                    ['type' => 'checkbox', 'label' => 'Mostrar ano letivo', 'name' => 'anoLetivo'],
                ])
        );
        $registry->register(
            Resource::create('consentimento', 'Autorização de tratamento de dados pessoais', 'file-contract', 'resources::resources.consentimento')
                ->setOptions([
                    ['type' => 'checkbox', 'label' => 'Autorização para menores (< 16)', 'name' => 'menor'],
                ])
        );
        $registry->register(
            Resource::create('lista', 'Lista de membros', 'file-alt', 'resources::resources.lista')
                ->setGroupResource(true)
                ->setOptions([
                    ['type' => 'checkbox', 'label' => 'Mostrar ano letivo', 'name' => 'anoLetivo'],
                    ['type' => 'checkbox', 'label' => 'Ocultar responsáveis', 'name' => 'semResponsaveis'],
                    ['type' => 'checkbox-group', 'label' => 'Campos', 'name' => 'fields', 'options' => [
                        'data_nascimento' => 'Data de nascimento',
                        'idade' => 'Idade',
                        'nic' => 'Número de Identificação Civil',
                        'nif' => 'Número de Identificação Fiscal',
                        'nus' => 'Numero de Utente de Saúde',
                        'email' => 'Endereço de e-mail',
                        'telefone' => 'Telefone',
                        'profissao' => 'Profissão',
                        'genero' => 'Género',
                    ]],
                ])
        );
    }
}
