<?php

namespace Sautor\Resources;

use Illuminate\Support\Collection;
use Sautor\Core\Models\Grupo;

class Resources
{
    private Collection $registeredResources;

    /**
     * AddonService constructor.
     */
    public function __construct()
    {
        $this->registeredResources = new Collection;
    }

    public function all(): Collection
    {
        return $this->registeredResources->sortBy(function ($addon) {
            return strtolower($addon->label);
        });
    }

    public function get(string $key): ?Resource
    {
        return $this->registeredResources->firstWhere('key', $key);
    }

    public function common(): Collection
    {
        return $this->all()->filter(fn (Resource $resource) => ! $resource->isGroupResource() && $resource->isVisibleFor());
    }

    public function group(Grupo $grupo): Collection
    {
        return $this->all()->filter(fn (Resource $resource) => $resource->isGroupResource() && $resource->isVisibleFor($grupo));
    }

    public function register(Resource $resource)
    {
        $this->registeredResources->push($resource);
    }
}
