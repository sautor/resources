<?php

namespace Sautor\Resources\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Sautor\Core\Http\Controllers\Controller;
use Sautor\Core\Models\Grupo;
use Spatie\Browsershot\Browsershot;

class ResourcesController extends Controller
{
    public function grupo(Grupo $grupo)
    {
        $registry = app()->make('resources');
        $resources = $registry->all();
        $commonResources = $registry->common();
        $groupResources = $registry->group($grupo);

        return view('resources::grupo', compact('grupo', 'resources', 'commonResources', 'groupResources'));
    }

    public function resource(Request $request, $key, ?Grupo $grupo = null)
    {
        $registry = app()->make('resources');
        $resource = $registry->get($key);
        if (! $resource) {
            abort(404);
        }
        $query = $request->query();

        $html = View::make($resource->view, compact('grupo', 'query'))->render();

        $pdf = Browsershot::html($html)
            ->format($resource->getFormat($query))
            ->waitUntilNetworkIdle()
            ->margins(10, 10, 10, 10)
            ->newHeadless()
            ->noSandbox()
            ->showBackground();

        return response($pdf->pdf(), 200, [
            'Content-Type' => 'application/pdf',
            'Content-Disposition' => 'inline',
        ]);
    }
}
